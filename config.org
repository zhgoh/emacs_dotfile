#+TITLE: My Emacs config
#+AUTHOR: Goh Zi He
#+STARTUP: overview
* Starters
This are some of the settings that most people will put in their new emacs settings.
** Make Emacs looks more modern
#+BEGIN_SRC emacs-lisp
  (menu-bar-mode -1)   ;; Hide menu bar
  (tool-bar-mode -1)   ;; Hide tool bar
  (tooltip-mode -1)    ;; Disable tooltips
  (scroll-bar-mode -1) ;; Disable scroll bar
  (set-fringe-mode 10) ;; Give some breathing room
#+END_SRC
 
** Set some sensible defaults
#+BEGIN_SRC emacs-lisp
  (setq-default
  truncate-lines t            ;; Word wrap to none
  indent-tabs-mode nil        ;; Use spaces instead of tabs
  tab-width 2                 ;; Set tab size to 2
  line-spacing 0)

  ;; Some basic things.
  (setq
  inhibit-startup-message t           ;; Don't show the startup message
  inhibit-startup-screen t            ;; or screen
  cursor-in-non-selected-windows t    ;; Hide the cursor in inactive windows
  echo-keystrokes 0.1                 ;; Show keystrokes right away, don't show the
                                      ;; message 
                                      ;; in the scratch buffer
  initial-scratch-message nil         ;; Empty scratch buffer
  sentence-end-double-space nil       ;; Sentences should end in one space, come on!
  visible-bell t                      ;; Enable visible bell
  user-full-name "Zi He Goh"          ;; Set user name
  user-mail-address "zihe.goh@gmail.com" ;; Set user email
  frame-title-format "%b (%f)")       ;; Show filename in title bar
  ;;confirm-kill-emacs 'y-or-n-p)      ;; y and n instead of yes and no when quitting

  ;; Never use tabs, use spaces instead.
  (setq-default
  indent-tabs-mode nil
  c-basic-indent 2
  c-basic-offset 2
  tab-width 2)

  (setq
  tab-width 2
  js-indent-level 2
  css-indent-offset 2
  c-basic-offset 2)

  (setq
  make-backup-files nil      ;; stop creating backup~ files
  auto-save-default nil      ;; stop creating #autosave# files
  create-lockfiles nil)      ;; stop creating .# files

  (show-paren-mode)           ;; Show matching paren
  (column-number-mode)        ;; Enable column line number in modeline
  (desktop-save-mode 1)       ;; Save last session
  (delete-selection-mode)
  (global-display-line-numbers-mode)    ;; Display line number
  (global-visual-line-mode)             ;; Enable truncated line
  (global-hl-line-mode)                 ;; Set current line highlight
  (global-auto-revert-mode) ;; Revert (update) buffers automatically when underlying
                          ;; files are changed externally.
  ;(windmove-default-keybindings 'meta)  ;; Set the prefix for window move to meta,
                                      ;; so alt+arrow will switch window.
  ;(blink-cursor-mode 0)
  (fset 'yes-or-no-p 'y-or-n-p)         ;; y and n instead of yes and no everywhere else
  ;; (setq indent-line-function 'insert-tab)
  ;; (setq c-default-style "linux")
  ;; (setq c-basic-offset 2)
  ;; (c-set-offset 'comment-intro 0)
  ;; (setq x-select-enable-clipboard-manager nil)   ;; Disable clipboard

  ;; Disable line numbers for some modes
  (dolist (mode '(org-mode-hook
                  term-mode-hook
                  shell-mode-hook
                  eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

#+END_SRC

** Set fonts
#+BEGIN_SRC emacs-lisp
  (add-to-list 'default-frame-alist '(font . "Fira Code 13"))    ;; Font settings
  (set-face-attribute 'default t :font "Fira Code 13")
  (set-face-attribute 'show-paren-match nil :weight 'extra-bold)
  (set-face-background 'show-paren-match "wheat")
  ;;(set-face-background 'hl-line "#fccf05")      ;; To customize the highlight color
  ;;(set-face-foreground 'highlight nil)          ;; Keep syntax highlighting in current line
#+END_SRC

** Keybindings
*** Escape works on all mode
#+BEGIN_SRC emacs-lisp
  ;; Make escape quit prompt
  (global-set-key (kbd "<escape>") 'keyboard-escape-quit)

#+END_SRC

*** Zooming in and out
You can use the binding CTRL plus =/- for zooming in and out
#+BEGIN_SRC emacs-lisp
  (global-set-key (kbd "C-=") 'text-scale-increase)
  (global-set-key (kbd "C--") 'text-scale-decrease)
  ;; (global-set-key (kbd "C-0") '(lambda () (interactive) (text-scale-adjust 0)))
  (global-set-key (kbd "<C-wheel-up>") 'text-scale-decrease)
  (global-set-key (kbd "<C-wheel-down>") 'text-scale-increase)
#+END_SRC

*** Reload emacs file
#+BEGIN_SRC emacs-lisp
  (global-set-key (kbd "<C-f5>") '(lambda () (interactive) (load-file "~/.emacs.d/init.el")))
#+END_SRC

* Packages
** Management
Using Straight as package manager

#+BEGIN_SRC emacs-lisp
  (setq straight-repository-branch "develop")
  (defvar bootstrap-version)
  (let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))

  (setq straight-use-package-by-default t)  ;; Will make use of straight in use-package
  (straight-use-package 'use-package)       ;; Install use-package
#+END_SRC

** Theming 
*** gruvbox
#+BEGIN_SRC emacs-lisp
  (use-package gruvbox-theme
    :config
    (load-theme 'gruvbox-light-hard t))
  ;; (use-package nord-theme
  ;;   :config
  ;;   (load-theme 'nord t))
#+END_SRC

*** all-the-icons
#+BEGIN_SRC emacs-lisp
  (use-package all-the-icons)             ;; First time, do M-x all-the-icons-install-font
#+END_SRC

*** doom-modeline
#+BEGIN_SRC emacs-lisp
  (use-package doom-modeline
    :init (doom-modeline-mode 1)
    :custom ((doom-modeline-height 15) (doom-modeline-buffer-file-name-style 'file-name)))
#+END_SRC

** Productivity
*** Beancount
#+BEGIN_SRC emacs-lisp
  (straight-use-package
   '(beancount-mode :type git :host github :repo "beancount/beancount-mode"))

  ;;(use-package beancount-mode
  ;;  :straight (beancount-mode :type git :host github :repo "beancount/beancount-mode"))
  ;;                                        ;:config
  ;;                                        ;(add-to-list 'load-path "~/.emacs.d/straight/repos/beancount-mode/"))
  ;;:config
  ;;(add-to-list 'auto-mode-alist '("\\.beancount\\'" . beancount-mode)))
#+END_SRC

*** Dashboard
#+BEGIN_SRC emacs-lisp
    (use-package dashboard
      :init
      ;; Set the title
      (setq dashboard-banner-logo-title "Welcome to Emacs Dashboard")
      ;; Set the banner
      (setq dashboard-startup-banner 2)
      ;; Value can be
      ;; 'official which displays the official emacs logo
      ;; 'logo which displays an alternative emacs logo
      ;; 1, 2 or 3 which displays one of the text banners
      ;; "path/to/your/image.png" or "path/to/your/text.txt" which displays whatever image/text
      ;; you would prefer

      ;; Content is not centered by default. To center, set
      (setq dashboard-center-content t)

      ;; To disable shortcut "jump" indicators for each section, set
      ;;(setq dashboard-show-shortcuts nil)
      (setq dashboard-items '((recents  . 5)
                              (bookmarks . 5)
                              (projects . 5)
                              (agenda . 5)
                              (registers . 5)))
      :config
      (dashboard-setup-startup-hook))
#+END_SRC

*** Evil
#+BEGIN_SRC emacs-lisp
  (use-package evil
    :demand t
    :init
    (setq evil-want-integration t)
    (setq evil-want-keybinding nil)
    (setq evil-want-C-u-scroll t)
    (setq evil-want-C-i-jump nil)
    :config
    (evil-mode t)
    (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
    (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

    (evil-global-set-key 'motion "j" 'evil-next-visual-line)
    (evil-global-set-key 'motion "k" 'evil-previous-visual-line))

  (use-package evil-collection
    :after evil magit
    :config
    (evil-collection-init))

  (use-package general
    :after evil
    :config
    (general-evil-setup t))
                                          ;(general-create-definer rune/leader-keys
                                          ;  :keymaps '(normal insert visual emacs)
                                          ;  :prefix "SPC"
                                          ;  :global-prefix "C-SPC"))

                                          ;(rune/leader-keys
                                          ;  "t" '(:ignore t :which-key "toggles")))


#+END_SRC
   
*** Helm mode
#+BEGIN_SRC emacs-lisp
  (use-package helm
    :demand t
    :bind (("M-x" . helm-M-x)
           ("C-x b" . helm-buffers-list)))
#+END_SRC

*** Hydra mode
#+BEGIN_SRC emacs-lisp
  (use-package hydra
    :config
    (defhydra hydra-text-scale (:timeout 4)
      "scale text"
      ("j" text-scale-increase "in")
      ("k" text-scale-decrease "out")
      ("f" nil "finished" :exit t)))

     ;(rune/leader-keys
     ;  "ts" '(hydra-test-scale/body :which-key "scale-text")
#+END_SRC

*** Keychord
#+BEGIN_SRC emacs-lisp
  ;;Exit insert mode by pressing j and then j quickly
  (use-package key-chord
    :after evil
    :init
    (setq key-chord-two-keys-delay 0.5)
    :config
    (key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
    (key-chord-mode 1))
#+END_SRC
   
*** Magit
It's Magic of course
#+BEGIN_SRC emacs-lisp
  (use-package magit
    :custom
    (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))
#+END_SRC

*** Org
#+BEGIN_SRC emacs-lisp
  (defun efs/org-mode-setup ()
    (org-indent-mode)
   ;(variable-pitch-mode 1)
    (visual-line-mode 1))

  (use-package org
    :hook (org-mode . efs/org-mode-setup)
    :config
    (setq org-ellipsis " ▾"))

  (use-package org-bullets
    :after org
    :hook (org-mode . org-bullets-mode)
    :custom
    (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

  (defun efs/org-mode-visual-fill ()
    (setq
     visual-fill-column-width 100
     visual-fill-column-center-text t)
    (visual-fill-column-mode 1))

  (use-package visual-fill-column
    :hook (org-mode . efs/org-mode-visual-fill))
#+END_SRC

*** Projectile
#+BEGIN_SRC emacs-lisp
  ;; Use Projectile for project management.
  (use-package projectile
    :after helm
    :diminish projectile-mode
    :config
    (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
    (projectile-mode)
                                          ;:custom ((projectile-completion-system 'helm))
    :init
    (setq projectile-switch-project-action #'projectile-dired))

#+END_SRC

*** Rainbow delimiters
#+BEGIN_SRC emacs-lisp
  (use-package rainbow-delimiters
    :hook (prog-mode . rainbow-delimiters-mode))
#+END_SRC
   
*** Which Key
#+BEGIN_SRC emacs-lisp
  (use-package which-key
    :init (which-key-mode)
    :diminish which-key-mode
    :config
    (setq which-key-idle-delay 0.3))
#+END_SRC

** Language
*** Go mode
#+BEGIN_SRC emacs-lisp

  (use-package lsp-mode
    :custom ((lsp-inhibit-message t)
             (lsp-message-project-root-warning t)
             (create-lockfiles nil))
    :hook
    (prog-major-mode . lsp-prog-major-mode-enable)
    :config
    (setq lsp-response-timeout 5))
  (add-hook 'hack-local-variables-hook
            (lambda () (when (derived-mode-p 'go-mode) (lsp))))

  ;; company-lsp integrates company mode completion with lsp-mode.
  ;; completion-at-point also works out of the box but doesn't support snippets.
  (use-package company-lsp
    :commands company-lsp)

  ;; Company mode is a standard completion package that works well with lsp-mode.
  (use-package company
    :config
    (global-company-mode)
    ;; Optionally enable completion-as-you-type behavior.
    (setq company-idle-delay 0)
    (setq company-minimum-prefix-length 1)
    (setq completion-ignore-case t)
    (setq company-dabbrev-downcase nil)
    (setq company-selection-wrap-around t))

  ;; go-mode
  (use-package go-mode
    :commands go-mode
    :bind (
           ;; If you want to switch existing go-mode bindings to use lsp-mode/gopls instead
           ;; uncomment the following lines
           ;; ("C-c C-j" . lsp-find-definition)
           ;; ("C-c C-d" . lsp-describe-thing-at-point)
           )
    :config
    (setq gofmt-command "goimports")
    (add-hook 'before-save-hook 'gofmt-before-save))

#+END_SRC

